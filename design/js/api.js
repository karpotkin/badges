    var badges = {};
    (function() {

        initApp = function() {
            storeId = '{{store_identifier}}';
            str = badges.$('html').html();
            theme_id = str.match(/theme_assets\/[0-9]{1,6}/ig);
            theme_id_clear = 0;
            if (theme_id != null) {
                var modeMap = {};
                var theme_id_clear = theme_id[0];
                maxCount = 1;
                for (var i = 0; i < theme_id.length; i++) {
                    var el = theme_id[i];
                    if (modeMap[el] == null)
                        modeMap[el] = 1;
                    else
                        modeMap[el] ++;
                    if (modeMap[el] > maxCount) {
                        theme_id_clear = el;
                        maxCount = modeMap[el];
                    }
                }
            }
            badges.$.ajax({
                url: 'api/get',
                type: 'GET',
                cache: false,
                async: true,
                crossDomain: true,
                dataType: 'json',
                data: {
                    'storeId': storeId,
                    'themeId': theme_id_clear
                },
                success: function(data) {
                    if (data != "") {
                        theme_not_id = true;
                        for (var i = 0; i < data.length; i++) {
                            page = "";
                            if (window.location.pathname.substr(1) == data[i]['slu']) {
                                page = "category";
                            }

                            if (data[i]['theme_id'] == 122675) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('.product_images a[href*="' + data[i]['img_url'] + '"]').append(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().append(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('.product_images a[href*="' + data[i]['img_url'] + '"]').append(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().append(data[i]['data']);
                                    }
                                }
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                                if (page == "category") {
                                    var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                    var st_sz = badges.$(window).width();
                                    if (badges.$(window).height() < 700)
                                        badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('width', "242");
                                    if (badges.$(window).height() > 700)
                                        badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('width', "404");

                                    //badges.$('a[href*="'+ data[i]['img_url'] + '"] .ov_h').css('width', img_sz);
                                    badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('margin-left', $("#product_info").outerWidth(true));
                                    badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('margin-top', $(".logotype-container").outerHeight(true));


                                    var hr = data[i]['img_url'];
                                    badges.$(window).resize(function() {
                                        if (badges.$('a[href*="' + hr + '"] img').width() != img_sz) {
                                            var img_sz = badges.$('img[src*="' + hr + '"]').width();
                                            var st_sz = badges.$(window).width();
                                            if (badges.$(window).height() < 700)
                                                badges.$('a[href*="' + hr + '"] .ov_h').css('width', "242");

                                            if (badges.$(window).height() > 700)
                                                badges.$('a[href*="' + hr + '"] .ov_h').css('width', "404");

                                            //badges.$('a[href*="'+ hr + '"] .ov_h').css('width', img_sz);
                                            badges.$('a[href*="' + hr + '"] .ov_h').css('margin-left', $("#product_info").outerWidth(true));
                                            badges.$('a[href*="' + hr + '"] .ov_h').css('margin-top', $(".logotype-container").outerHeight(true));
                                        }
                                    });

                                    var hm = data[i]['slu'];
                                    $('.content_container').on('scroll', function() {
                                        $('a[href*="' + hr + '"] .ov_h').css('margin-left', $('.product_info').width() + 60 - $('.content_container').scrollLeft());
                                    });
                                }
                            }
                            if (data[i]['theme_id'] == 771) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('.image_slide a[href*="' + data[i]['img_url'] + '"]').prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('.image_slide a[href*="' + data[i]['img_url'] + '"]').prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().prepend(data[i]['data']);
                                    }
                                }
                            }

                            if (data[i]['theme_id'] == 115849) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('.main-image a[href*="' + data[i]['img_url'] + '"]').prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"]').prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('.main-image a[href*="' + data[i]['img_url'] + '"]').prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"]').prepend(data[i]['data']);
                                    }
                                }
                                if (page == "category") {
                                    var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                    var st_sz = badges.$(window).width();
                                    badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('width', img_sz);
                                    badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('margin-left', (st_sz / 2 - img_sz / 2));
                                    var hr = data[i]['img_url'];
                                    badges.$(window).resize(function() {
                                        if (badges.$('a[href*="' + hr + '"] img').width() != img_sz) {
                                            var img_sz = badges.$('img[src*="' + hr + '"]').width();
                                            var st_sz = badges.$(window).width();
                                            badges.$('a[href*="' + hr + '"] .ov_h').css('width', img_sz);
                                            badges.$('a[href*="' + hr + '"] .ov_h').css('margin-left', (st_sz / 2 - img_sz / 2));
                                        }
                                    });
                                }
                            }

                            if (data[i]['theme_id'] == 113093) {
                                theme_not_id = false;
                                badges.$('.image_container img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                            }

                            if (data[i]['theme_id'] == 85405) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] figure').prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] figure').prepend(data[i]['data']);
                                    }
                                }
                                var img_sz = badges.$('a[href*="' + data[i]['slu'] + '"] img').width();
                                badges.$('a[href*="' + data[i]['slu'] + '"] .ov_h').css('left', '50%');
                                badges.$('a[href*="' + data[i]['slu'] + '"] .ov_h').css('width', img_sz);
                                badges.$('a[href*="' + data[i]['slu'] + '"] .ov_h').css('margin-left', img_sz / 2 * -1);
                                var hr = data[i]['slu'];
                                badges.$(window).resize(function() {
                                    if (badges.$('a[href*="' + hr + '"] img').width() != img_sz) {
                                        img_sz = badges.$('a[href*="' + hr + '"] img').width();
                                        badges.$('a[href*="' + hr + '"] .ov_h').css('left', '50%');
                                        badges.$('a[href*="' + hr + '"] .ov_h').css('width', img_sz);
                                        badges.$('a[href*="' + hr + '"] .ov_h').css('margin-left', img_sz / 2 * -1);
                                    }
                                });
                            }

                            if (data[i]['theme_id'] == 85395) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().parent().parent().parent().prepend(data[i]['data']);
                                        //badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().append(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        //badges.$('.ov_h').wrap("<div class='category'></div>");
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().append(data[i]['data']);
                                    }
                                }
                            }

                            if (data[i]['theme_id'] == 82222) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().parent().parent().parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().append(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().append(data[i]['data']);
                                    }
                                }

                                if (page == "category") {
                                    var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                    var st_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').outerHeight();
                                    badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('left', '50%');
                                    badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('width', img_sz);
                                    badges.$('a[href*="' + data[i]['img_url'] + '"] .ov_h').css('margin-left', (img_sz / 2 * -1));
                                    var hr = data[i]['img_url'];
                                    badges.$(window).resize(function() {
                                        if (badges.$('a[href*="' + hr + '"] img').width() != img_sz) {
                                            img_sz = badges.$('img[src*="' + hr + '"]').width();
                                            badges.$('a[href*="' + hr + '"] .ov_h').css('left', '50%');
                                            badges.$('a[href*="' + hr + '"] .ov_h').css('width', img_sz);
                                            badges.$('a[href*="' + hr + '"] .ov_h').css('margin-left', (img_sz / 2 * -1));
                                        }
                                    });
                                }


                            }

                            if (data[i]['theme_id'] == 77561) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().append(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().append(data[i]['data']);
                                    }
                                }
                            }

                            if (data[i]['theme_id'] == 75588) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().append(data[i]['data']);
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().css('overflow', 'visible');
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().append(data[i]['data']);
                                    }
                                }

                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                                var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                badges.$('a[href*="' + data[i]['slu'] + '"] .ov_h').css('width', img_sz);
                                badges.$('a[href*="' + data[i]['slu'] + '"] .ov_h').css('margin-top', "13%");
                                var hr = data[i]['img_url'];
                                var sl = data[i]['slu'];
                                badges.$(window).resize(function() {
                                    if (badges.$('a[href*="' + hr + '"] img').width() != img_sz) {
                                        img_sz = badges.$('img[src*="' + hr + '"]').width();
                                        badges.$('a[href*="' + sl + '"] .ov_h').css('width', img_sz);
                                    }
                                });

                                if (page == "category") {
                                    var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                    badges.$('.category_75588').css('left', '50%');
                                    badges.$('.category_75588').css('width', img_sz);
                                    badges.$('.category_75588').css('margin-left', (img_sz / 2 * -1));
                                    var hr = data[i]['img_url'];
                                    badges.$(window).resize(function() {
                                        if (badges.$('a[href*="' + hr + '"] img').width() != img_sz) {
                                            img_sz = badges.$('img[src*="' + hr + '"]').width();
                                            badges.$('.category_75588').css('width', img_sz);
                                            badges.$('.category_75588').css('margin-left', (img_sz / 2 * -1));
                                        }
                                    });
                                }
                            }

                            if (data[i]['theme_id'] == 73210) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').eq(1).parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').eq(1).addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').eq(1).parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').eq(1).addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                }


                                badges.$('.ov_h').css('position', 'relative');
                                var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                var img_hg = badges.$('img[src*="' + data[i]['img_url'] + '"]').height();
                                badges.$('.elem_3_' + data[i]['theme_id']).css('width', img_sz);
                                badges.$('.elem_1_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().css('overflow', 'visible');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().parent().css('overflow', 'hidden');
                                badges.$('.elem_2_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('top', '115px');
                                badges.$('.elem_3_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                                //badges.$('.elem_3_'+data[i]['theme_id']).css('top', img_hg*-1);
                            }

                            if (data[i]['theme_id'] == 6228) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                }


                                badges.$('.ov_h').css('position', 'relative');
                                var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                var img_hg = badges.$('img[src*="' + data[i]['img_url'] + '"]').height();
                                badges.$('.elem_3_' + data[i]['theme_id']).css('width', img_sz);


                                badges.$('.elem_1_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().css('overflow', 'visible');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().parent().css('overflow', 'hidden');
                                badges.$('.elem_2_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('top', '115px');
                                badges.$('.elem_3_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                            }

                            if (data[i]['theme_id'] == 1839) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().prepend(data[i]['data']);
                                    }
                                }
                                badges.$('.ov_h').css('position', 'relative');
                                badges.$('.ov_h').css('height', '0');
                                var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                var img_hg = badges.$('img[src*="' + data[i]['img_url'] + '"]').height();
                                badges.$('.elem_3_' + data[i]['theme_id']).css('width', img_sz);
                                badges.$('.elem_1_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().css('overflow', 'visible');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().parent().css('overflow', 'hidden');
                                badges.$('.elem_2_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('top', '115px');
                                badges.$('.elem_3_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                            }

                            if (data[i]['theme_id'] == 1806) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().css('overflow', 'visible');
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().append(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().append(data[i]['data']);
                                    }
                                }
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                            }

                            if (data[i]['theme_id'] == 159) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().prepend(data[i]['data']);
                                    }
                                }
                                badges.$('.ov_h').css('position', 'relative');
                                badges.$('.ov_h').css('height', '0');
                                var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                var img_hg = badges.$('img[src*="' + data[i]['img_url'] + '"]').height();
                                badges.$('.elem_3_' + data[i]['theme_id']).css('width', img_sz);
                                badges.$('.elem_1_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().css('overflow', 'visible');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().parent().css('overflow', 'hidden');
                                badges.$('.elem_2_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('top', '115px');
                                badges.$('.elem_3_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                            }

                            if (data[i]['theme_id'] == 1) {
                                theme_not_id = false;
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().parent().prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').addClass('category_' + data[i]['theme_id']);
                                    } else {
                                        badges.$('a[href*="' + data[i]['slu'] + '"] img').parent().prepend(data[i]['data']);
                                    }
                                }
                                badges.$('.ov_h').css('position', 'relative');
                                badges.$('.ov_h').css('height', '0');
                                var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                var img_hg = badges.$('img[src*="' + data[i]['img_url'] + '"]').height();
                                badges.$('.elem_3_' + data[i]['theme_id']).css('width', img_sz);
                                badges.$('.elem_1_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().css('overflow', 'visible');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().parent().css('overflow', 'hidden');
                                badges.$('.elem_2_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('top', '115px');
                                badges.$('.elem_3_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                            }

                            if (theme_not_id) {
                                if (data[i]['id'] == 2) {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category_231'></div>");
                                    } else {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                    }
                                } else {
                                    if (page == "category") {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                        badges.$('.ov_h').wrap("<div class='category231'></div>");
                                    } else {
                                        badges.$('img[src*="' + data[i]['img_url'] + '"]').parent().prepend(data[i]['data']);
                                    }
                                }
                                badges.$('.ov_h').css('position', 'relative');
                                badges.$('.ov_h').css('height', '0');
                                var img_sz = badges.$('img[src*="' + data[i]['img_url'] + '"]').width();
                                var img_hg = badges.$('img[src*="' + data[i]['img_url'] + '"]').height();
                                badges.$('.elem_3_' + data[i]['theme_id']).css('width', img_sz);
                                badges.$('.elem_1_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().css('overflow', 'visible');
                                badges.$('.elem_1_' + data[i]['theme_id']).parent().parent().css('overflow', 'hidden');
                                badges.$('.elem_2_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('top', '115px');
                                badges.$('.elem_3_' + data[i]['theme_id']).css('position', 'absolute');
                                badges.$('.elem_2_' + data[i]['theme_id']).parent().css('z-index', '2');
                            }

                        }
                    }
                }
            })
        };


        if (!TT.hasjQuery('1.11.1')) {
            TT.loadScript(
                'tt-badges-jq',
                window.location.protocol + '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
                function(id, src) {
                    badges.$ = jQuery.noConflict(true);
                    initApp();
                }
            );
        } else {
            badges.$ = jQuery;
            initApp();
        }
    })();
