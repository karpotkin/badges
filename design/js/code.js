$(document).ready(function() {

    var selectedItems = new Array(),
        inp;

    $('input[name*="t_check"]:checked').map(function() { selectedItems.push($(this).val()); } );


    $("#check_val").val(selectedItems);
    $('#ajax_search').on('ifChecked', 'input[name*="t_check"]', function(event) {
        selectedItems.push($(this).val());
        if($(this).hasClass('m_o')){
            inp = this.value;
            $('#modal-check').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('.a_chc').trigger('click');
        }
        $("#check_val").val(selectedItems);
    });

    $('#ajax_search').on('ifUnchecked', 'input[name*="t_check"]', function(event) {
        selectedItems.splice(selectedItems.indexOf($(this).val()), 1);
        jQuery("#check_val").val(selectedItems);
    });

    $(".no_use").click(function() {
        selectedItems.splice(selectedItems.indexOf($(this).val()), 1);
        jQuery("#check_val").val(selectedItems);
        jQuery.ajax({
            type: 'post',
            url: 'ajax/updateBadge.php',
            data: {
                productId: inp,
                storeId: jQuery("#storeid").val(),
                accessToken: jQuery("#access_token").val()
            },
            cache: false,
            async: true,
            success: function(html) {
                console.log(html);
            }
        });
    });

    $(".ckd_fl").click(function() {
        $('input[value="' + inp + '"]').iCheck('uncheck');
    });


    $(".badge-template__item_js").next()
        .find('input:checked')
        .parent()
        .prev()
        .css({
            'background-color': $("#b_color_n").val(),
            'color': $("#t_color_n").val()
        });


    $('.badge-template_js').on('click', function() {
        $('.badge-template__item_js').css({
                'background-color': '#e8e7e7',
                'color': '#fff'
            });
        $(this).find('.badge-template__item_js').css({
                'background-color': $("#b_color_n").val(),
                'color': $("#t_color_n").val()
            });
    });


    $("#title").on('input', function() {
        $('[data-change="title"]').text($(this).val());
    });

    function chacheColor(elem, type){
        $(".badge-template_js").find('input:checked')
            .parent()
            .prev()
            .css(type, elem.val());
    }

    $("#b_color_n").change(function() {
        chacheColor($(this), 'background-color');
    });

    $("#t_color_n").change(function() {
        chacheColor($(this), 'color');
    });

    function searchRequest(){
        $.ajax({
            type: 'POST',
            url: 'ajax/search.php',
            data: {
                s: $("#search").val(),
                select_cat: $("#select_cat").val(),
                storeId: $("#storeid").val(),
                id: $("#id_m").val(),
                selected_item: selectedItems
            },
            cache: false,
            async: true,
            success: function(html) {
                if (html != "") {
                    $(" #ajax_search ").empty().append(html);
                    $('input[name*="t_check"]').iCheck();
                }
            }
        });
    }
    $("#search").on('input', function() {
        searchRequest();
    });

    $("#select_cat").change(function() {
        searchRequest();
    });
});

jQuery(document).ready(function() {
    $('#b_color').ColorPicker({
        color: '#ff0000',
        onShow: function(colpkr) {
            $(colpkr).fadeIn(500);
            return false;
        },
        onHide: function(colpkr) {
            $(colpkr).fadeOut(500);
            return false;
        },
        onChange: function(hsb, hex, rgb) {
            $('#b_color').css('backgroundColor', '#' + hex);
            $('#b_color_n').val('#' + hex).change();
        }
    });
    $('#t_color').ColorPicker({
        color: '#ff0000',
        onShow: function(colpkr) {
            $(colpkr).fadeIn(500);
            return false;
        },
        onHide: function(colpkr) {
            $(colpkr).fadeOut(500);
            return false;
        },
        onChange: function(hsb, hex, rgb) {
            $('#t_color').css('backgroundColor', '#' + hex);
            $('#t_color_n').val('#' + hex).change();
        }
    });
});
