{extends file='layout.tpl'}
{block name='main'}
    <h1 class="pull-left">{$page_name}</h1>
    <a href="badges" class="pull-right">
        <button type="button" class="ui right pull-right" id="add-product-button">Create Badge</button>
    </a>
    <hr>
    <div class="loading-data text-muted text-center">Please wait while loading data...</div>
    <div class="row-fluid" id="app_content"></div>
{/block}