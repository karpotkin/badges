<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{$meta_title|escape}</title>
    <link rel="stylesheet" href="https://sdk.ttcdn.co/tt-uikit-0.11.0.min.css">
    <link rel="stylesheet" href="/design/css/style.css?{math equation='rand(10000,20000)'}">
    {block name='headCSS'}{/block}
</head>
<body>
<div id="page" class="appsize">
    {block name='main'}{/block}
</div>
<!-- jQuery (this is required) -->
<script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
<!-- Tictail UIKit JavaScript -->
<script src="//sdk.ttcdn.co/tt-uikit-0.11.0.min.js"></script>
<!-- Tictail JavaScript library -->
<script src="//sdk.ttcdn.co/tt.js"></script>

{block name='footerScript'}{/block}
{literal}
    <script>
        jQuery( document ).ready(function() {
            TT.native.loading();
            TT.native.init()
                .done(init)
                .fail(genericError);
            function init() {
                TT.api.get('v1/me')
                    .done(function(store) {
                        storeId = store.id;
                        token = TT.native.accessToken;

                        if(storeId != "" && token != "") {

                            $.ajax({
                                url: 'ajax/load_data.php',
                                type: 'POST',
                                data: {
                                    'storeId': storeId,
                                    'accessToken': token
                                },
                                success: function(data) {
                                    $("#app_content").append(data);

                                    $(".loading-data").hide();
                                    $("#app_content").show();
                                }
                            })
                        }
                    })
                    .fail(genericError);

            }
            function genericError(e) {
                console.error(e);
            }
            $( window ).load(function() {
                TT.native.loaded()
            });
        });
    </script>
{/literal}
</body>
</html>