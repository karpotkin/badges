{extends file='layout.tpl'}
{assign var="no_chk"}
{block name='headCSS'}
    <link rel="stylesheet" href="/design/css/colorpicker.css">
    <link rel="stylesheet" href="/design/css/products.css">
{/block}
{block name='main'}
    <div class="box clearfix">
        <h1 data-change="title" class="pull-left">{if $badge->id}{$badge->name}{else}New Badge{/if}</h1>
        <a class="pull-right mt15 _fw_bold" href="/">All badges</a>
    </div>
    <form data-validate method="post">
        <input type="hidden" value="{$badge->id}" name="id" id="id_m">
        <input type="hidden" value="{$badge->id_mag}" name="id_mag" id="storeid">
        <input type="hidden" value="{$token}" name="access_token" id="access_token">
        <fieldset class="row row_mod">
            <h4 class="column inner-left-column no-gutter-l">Info</h4>
            <div class="column inner-right-column no-gutter">
                <div class="row-fluid">
                    <div class="col-md-9 col-xs-9">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" autocomplete="off" type="text" name="title" id="title" value="{$badge->name}" minlength="1" required>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <label>Badge is active</label>
                        <div class="switch">
                            <input type="checkbox" {if $badge->enable}checked{/if} name="enable">
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <fieldset class="row row_mod">
            <h4 class="column inner-left-column no-gutter-l">Badge design</h4>
            <div class="column inner-right-column no-gutter">
                <div class="row-fluid">
                    <div class="col-md-12 col-xs-12 clearfix">
                        <label for="elem_1_c" class="pull-left badge-template_js">
                            <div class="block_d over">
                                <div data-change="title" class="elem_1 text_bk badge-template__item badge-template__item_js">
                                    {if $badge->id}{$badge->name}{else}New Badge{/if}
                                </div>
                                <input name="elem_r" id="elem_1_c elem_r" type="radio" value="1" {if $badge->elem == 1}checked="checked"{/if} required>
                            </div>
                        </label>
                        <label for="elem_2_c" class="pull-left badge-template_js">
                            <div class="block_d">
                                <div data-change="title" class="text_bk elem_2 badge-template__item badge-template__item_js">
                                    {if $badge->id}{$badge->name}{else}New Badge{/if}
                                </div>
                                <input name="elem_r" id="elem_2_c elem_r" type="radio" value="2" {if $badge->elem == 2}checked="checked"{/if} required>
                            </div>
                        </label>
                        <label for="elem_3_c" class="pull-left badge-template_js">
                            <div class="block_d over st_right">
                                <div data-change="title" class="elem_3 text_bk badge-template__item badge-template__item_js">
                                    {if $badge->id}{$badge->name}{else}New Badge{/if}
                                </div>
                                <input name="elem_r" id="elem_3_c elem_r" type="radio" value="3" {if $badge->elem == 3}checked="checked"{/if} required>
                            </div>
                        </label>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label for="title">Background color</label>
                            <input class="form-control" type="text" id="b_color" style="background-color: {if $badge->b_color}{$badge->b_color}{else}#ff6969{/if}">
                            <input name="b_color" type="hidden" value="{if $badge->b_color}{$badge->b_color}{else}#ff6969{/if}" id="b_color_n">
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="form-group">
                            <label for="title">Text color</label>
                            <input class="form-control" type="text" name="t_color" id="t_color" style="background-color: {if $badge->t_color}{$badge->t_color}{else}#fffff{/if}">
                            <input name="t_color" type="hidden" value="{if $badge->t_color}{$badge->t_color}{else}#ffffff{/if}" id="t_color_n">
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="box clearfix">
            <hr>
            <h1>Select products {if $badge->id}({$badge->counts}){/if}</h1>
            <hr>
        </div>
        <div class="select_c">
            <div class="form-inline row-flud clearfix">
                <div class="col-xs-3 col-md-3">
                    <select id="select_cat" name="select_cat">
                        <option value="all">All Products</option>
                        {foreach $categories as $category}
                            <option value="{$category->id}">{$category->title}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-xs-9 col-md-9">
                    <input type="search" placeholder="Search products" class="form-control search_mod" id="search">
                </div>
            </div>

            <table class="table table-striped">
                <thead>
                <tr>
                    <th class="title">Product</th>
                    <th class="qtu">Qty</th>
                    <th class="visible">Visible</th>
                    <th class="add_badge">Add badge</th>
                </tr>
                </thead>
                <tbody id="ajax_search">
                    {include file='partials/products.tpl'}
                </tbody>
            </table>
            <div class="actions">
                <ul class="inner-right-column inner-right-column_mod">
                    <li><button id="name-save" class="ui" type="submit" tabindex="999">Save</button></li>
                    <li><a id="name-link" href="/" class="manual" tabindex="999">Cancel and go back</a></li>
                    {if $badge->id}
                        <li class="pull-right"><a data-toggle="modal" href="#modal-delete" class="action-link delete-link">Delete this badge</a></li>
                    {/if}
                </ul>
            </div>
        </div>

        <input type="hidden" name="check_val" id="check_val">

        {include file='partials/popup-delete.tpl'}
    </form>

{/block}
{block name='footerScript'}
    <script src="/design/js/colorpicker.js"></script>
    <script src="/design/js/code.js?{math equation='rand(10000,20000)'}"></script>
{/block}