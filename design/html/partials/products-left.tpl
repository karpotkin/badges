{foreach $tictailProducts as $tp}
    <tr>
        <td>
            {if $tp->images[0]->url}
                <a href="#lightbox-{$tp->id}" data-toggle="modal" class="img_cat img_t" style="background-image: url('{$tp->images[0]->url}?size=100');"></a>

                <div id="lightbox-{$tp->id}" class="modal fade lightbox">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <img src="{$tp->images[0]->url}" alt="">
                        </div>
                    </div>
                </div>
            {/if}
            <a href="{$tp->store_url}/product/{$tp->slug}" target="_blank">{$tp->title}</a>
        </td>
        <td class="qtu">{if $tp->quantity}{$tp->quantity}{else}—{/if}</td>
        <td class="status visible">
            <span {if $tp->status == "published"}class="on"{/if}></span>
        </td>
        <td class="add_badge">
            {$no_chk = false}
            {foreach $productsNotInCurrent as $pnc}
                {if in_array($tp->id, $pnc->id_prod|unserialize)}
                    <input type="checkbox" name="t_check[]" class="m_o" value="{$tp->id}" {if $tp->id|in_array:$currentProducts}checked{/if}>
                    {$no_chk = true}
                    {break}
                {/if}
            {/foreach}
            {if !$no_chk}
                <input type="checkbox" id="checkbox-1" name="t_check[]" value="{$tp->id}" {if $tp->id|in_array:$currentProducts}checked{/if}>
            {/if}
        </td>
    </tr>
{/foreach}