<div id="modal-delete" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-left">
                Delete this badge?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger" name="del" value="true">Yes</button>
            </div>
        </div>
    </div>
</div>