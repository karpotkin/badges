{foreach $inArrayList as $al}
    <tr>
        <td>
            {if $tictailProducts[$al]->images[0]->url}
                <a href="#lightbox-{$tictailProducts[$al]->id}" data-toggle="modal" class="img_cat img_t" style="background-image: url('{$tictailProducts[$al]->images[0]->url}?size=100');"></a>

                <div id="lightbox-{$tictailProducts[$al]->id}" class="modal fade lightbox">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <img src="{$tictailProducts[$al]->images[0]->url}" alt="">
                        </div>
                    </div>
                </div>
            {/if}
            <a href="{$tictailProducts[$al]->store_url}/product/{$tictailProducts[$al]->slug}" target="_blank">
                {$tictailProducts[$al]->title}
            </a>
        </td>
        <td class="qtu">{if $tictailProducts[$al]->quantity}{$tictailProducts[$al]->quantity}{else}—{/if}</td>
        <td class="status visible">
            <span {if $tictailProducts[$al]->status == "published"}class="on"{/if}></span>
        </td>
        <td class="add_badge">
            <input type="checkbox" name="t_check[]" value="{$tictailProducts[$al]->id}" {if $tictailProducts[$al]->id|in_array:$currentProducts}checked{/if}>
        </td>
    </tr>
{/foreach}