{block name='headCSS'}
    <link rel="stylesheet" href="/design/css/badges.css">
{/block}
<div id="product-index-table" class="data-table">
    <table>
        <thead>
            <tr>
                <th class="title"><a href="#" class="title">Title</a></th>
                <th class="spacing"></th>
                <th class="time"><a href="#" class="time">Last Modified</a></th>
                <th class="quantity"><a href="#" class="quantity">Qty</a></th>
                <th class="status"><a href="#" class="status">Visible</a></th>
            </tr>
        </thead>
        <tbody>
            {foreach $badges as $i => $badge}
                <tr class="{if ($i+1) % 2 == 0}odd{else}even{/if}">
                    <td class="title">
                        <a href="/badges/{$badge->id}/{$badge->id_mag}{$token}" class="badge-thumb" style="background-color: {$badge->b_color}; color: {$badge->t_color};">
                            {$badge->name|substr:0:1}
                        </a>
                        <a href="/badges/{$badge->id}/{$badge->id_mag}{$token}">{$badge->name}</a>
                    </td>
                    <td class="spacing clickable"></td>
                    <td class="time clickable">
                        <time datetime="{$badge->modify_at}">{$badge->modify_at|date_format}</time>
                    </td>
                    <td class="quantity clickable">
                        <div class="inner">
                            <p>
                                {if $badge->counts}{$badge->counts}{else}—{/if}
                            </p>
                        </div>
                    </td>
                    <td class="status clickable">
                        <span {if $badge->enable}class="visible"{/if}></span>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>