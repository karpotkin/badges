<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 21/01/15
 * Time: 08:40
 */

require_once('Master.php');

class Badges extends Master{

    public function __construct()
    {
        parent::__construct();
    }

    public function getCurrentTheme($storeId, $token)
    {
        $curl = curl_init();
        @curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.tictail.com/v1/stores/'.$storeId.'/theme',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array("Authorization: Bearer ".mysql_escape_string($token))
        ));
        $theme = curl_exec($curl);
        @curl_close($curl);
        $theme = json_decode($theme);

        return $theme;
    }

    public function getTictailProducts($storeId)
    {
        $curl = curl_init();
        @curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.tictail.com/v1/stores/'.$storeId.'/products',
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $products = curl_exec($curl);
        @curl_close($curl);
        $products = json_decode($products);

        return $products;
    }

    public function getTictailStoreId($token)
    {
        $curl = curl_init();
        @curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.tictail.com/v1/me',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array("Authorization: Bearer ".mysql_escape_string($token))
        ));
        $me = curl_exec($curl);
        @curl_close($curl);

        $me = json_decode($me);

        return $me;
    }

    public function getBadgesByStoreId($id)
    {
        $query = $this->db->placehold("SELECT * FROM __badges WHERE id_mag = ?", $id);
        $this->db->query($query);

        return $this->db->results();
    }

    public function getBadgeById($id)
    {
        $query = $this->db->placehold("SELECT * FROM __badges WHERE id = ?", $id);
        $this->db->query($query);

        return $this->db->result();
    }

    public function addCountProductsOnBadges($badges, $products){

        foreach ($products as $p){
            $products_ids[] = $p->id;
        }

        foreach($badges as $key => $value){
            $badge_products = unserialize($value->id_prod);
            if(count($badge_products)) {
                $cnt = count(array_diff($badge_products, $products_ids));
                $badges[$key]->counts = $badges[$key]->counts - $cnt;
            }else{
                $badges[$key]->counts = 0;
            }
        }

        return $badges;
    }

    public function getProductsCategories($products){

        $categories = [];

        foreach($products as $i => $p){
            foreach($p->categories as $category){
                $categories[$i]->id = $category->id;
                $categories[$i]->title = $category->title;
            }
        }

        return $categories;
    }

    public function getProductsNotInCurrent($storeId, $id)
    {

        $query = $this->db->placehold("SELECT * FROM __badges WHERE id_mag= ? AND id <> ?", $storeId, $id);
        $this->db->query($query);

        return $this->db->results();
    }

    public function getCurrentProducts($badge)
    {
        return unserialize($badge->id_prod);
    }

    public function getLikeById($storeId, $productId){

        $query = $this->db->placehold('SELECT id, id_prod FROM __badges WHERE id_prod LIKE "%'.mysql_real_escape_string($productId).'%" AND id_mag = ? LIMIT 1', $storeId);
        $this->db->query($query);

        return $this->db->result();
    }

    public function addCountProductsOnBadge($badge, $products)
    {

        foreach ($products as $p){
            $products_ids[] = $p->id;
        }

        $badge_products = $this->getCurrentProducts($badge);
        if(count($badge_products)) {
            $cnt = count(array_diff($badge_products, $products_ids));
            $badge->counts = $badge->counts - $cnt;
        }else{
            $badge->counts = 0;
        }

        return $badge;
    }

    public function addBadge($badge)
    {
        $query = $this->db->placehold('INSERT INTO __badges SET ?%', $badge);

        if(!$this->db->query($query))
            return false;

        $id = $this->db->insert_id();

        return $id;
    }

    public function deleteBadge($id)
    {

        if(!empty($id)) {
            $query = $this->db->placehold("DELETE FROM __badges WHERE id = ? LIMIT 1", intval($id));
            if($this->db->query($query))
                return true;
        }
        return false;
    }

    public function updateBadge($id, $badge)
    {
        $badge->modify_at = date('Y-m-d H:i:s');
        $query = $this->db->placehold("UPDATE __badges SET ?% WHERE id = ? LIMIT 1", $badge, intval($id));
        if(!$this->db->query($query))
            return false;
        return $id;
    }

}