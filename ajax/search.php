<?php

chdir('..');
require_once('classes/Master.php');

$master = new Master();

if($master->request->method('post') && $storeId = $master->request->post('storeId'))
{
	$id = $master->request->post('id');

	$tictailProducts = $master->badges->getTictailProducts($storeId);

	$badge = $master->badges->getBadgeById($id);
	$categories = $master->badges->getProductsCategories($tictailProducts);
	$badge = $master->badges->addCountProductsOnBadge($badge, $tictailProducts);

	$currentProducts = $master->badges->getCurrentProducts($badge);

	$inArrayList = [];

	foreach($tictailProducts as $key => $tp){
		if(in_array($tp->id, $currentProducts)){
			$inArrayList[] = $key;
		}
	}

	$productsNotInCurrent = $master->badges->getProductsNotInCurrent($storeId, $id);

	$selected_item = $master->request->post('selected_item');

	$master->design->assign('categories', $categories);
	$master->design->assign('badge', $badge);
	$master->design->assign('tictailProducts', $tictailProducts);
	$master->design->assign('productsNotInCurrent', $productsNotInCurrent);
	$master->design->assign('currentProducts', $currentProducts);

	if(($master->request->post('s') == "") && ($master->request->post('select_cat') == "") || ($master->request->post('select_cat') == "all") && ($master->request->post('s') == ""))
	{
		$master->design->assign('inArrayList', $inArrayList);
		echo $master->design->fetch('partials/products-left.tpl');
		die();
	}

	if(($master->request->post('select_cat') != ""))
	{

		$select_cat = $master->request->post('select_cat');

		for($i = 0; $i < count($tictailProducts); $i++)
		{
			for($j = 0; $j < count($tictailProducts[$i]->categories); $j++) {
				if(substr_count(strtolower($tictailProducts[$i]->categories[$j]->id), strtolower($select_cat)) || $select_cat == "all")
				{
					$search_data[] = $i;
				}
			}

		}

		$master->design->assign('inArrayList', $search_data);
		echo $master->design->fetch('partials/products-left.tpl');
		die();
	}


	$search = $master->request->post('s');

	for($i = 0; $i < count($tictailProducts); $i++)
	{
		if(substr_count(strtolower($tictailProducts[$i]->title), strtolower($search)))
		{
			$search_data[] = $i;
		}

	}


	if(count($search_data) == 0)
		die("No result");

	$master->design->assign('inArrayList', $search_data);
	echo $master->design->fetch('partials/products-left.tpl');

}