<?php

chdir('..');
require_once('classes/Master.php');

$master = new Master();

if($master->request->method('post')){

	$storeId = $master->request->post('storeId');
	$token = $master->request->post('accessToken');

	$me = $master->badges->getTictailStoreId($token);
	$products = $master->badges->getTictailProducts($storeId);

	if($me->id == $storeId) {
		$badges = $master->badges->getBadgesByStoreId($storeId);

		$badges = $master->badges->addCountProductsOnBadges($badges, $products);
		$master->design->assign('token','?accessToken='.$token);
		$master->design->assign('badges', $badges);

		echo $master->design->fetch('partials/badge-card.tpl');

	}
}