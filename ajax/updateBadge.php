<?php

chdir('..');
require_once('classes/Master.php');

$master = new Master();

if($master->request->method('post') && $master->request->post('storeId') && $master->request->post('accessToken')){

    $storeId = $master->request->post('storeId');
    $productId = $master->request->post('productId');

    $badge = $master->badges->getLikeById($storeId, $productId);

    $badge_products = $master->badges->getCurrentProducts($badge);

    for($i = 0; $i < count($badge_products); $i++){
        if($badge_products[$i] == $productId)
        {
            unset($badge_products[$i]);
        }
    }

    $badge->counts = count($badge_products);
    $badge->id_prod = serialize(array_values($badge_products));

    return $master->badges->updateBadge($badge->id, $badge);
}