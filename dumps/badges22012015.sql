-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: badges
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `badges`
--

DROP TABLE IF EXISTS `badges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `b_color` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'ff0000',
  `t_color` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'ffffff',
  `enable` tinyint(1) NOT NULL DEFAULT '0',
  `counts` int(5) NOT NULL DEFAULT '0',
  `id_prod` text CHARACTER SET utf8 NOT NULL,
  `elem` int(2) NOT NULL DEFAULT '1',
  `id_mag` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `access_token` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badges`
--

LOCK TABLES `badges` WRITE;
/*!40000 ALTER TABLE `badges` DISABLE KEYS */;
INSERT INTO `badges` VALUES (18,'5','#000000','#ffffff',1,0,'',2,'',''),(45,'Top sale','#000000','#ffffff',0,0,'',3,'',''),(46,'best sale','#000000','#ffffff',1,0,'',3,'',''),(48,'HOT!','#ffc412','#ffffff',1,2,'a:2:{i:0;s:4:\"dn5R\";i:1;s:4:\"dn6j\";}',2,'366w','accesstoken_krY2h2chqoGx4VZmXcVMvsT1mSNjf7'),(49,'NEW','#ff4a4a','#ffffff',1,2,'a:2:{i:0;s:4:\"dn67\";i:1;s:4:\"dn6m\";}',1,'366w','accesstoken_iYZkp8iOVeDHAvMJFEq4ytnotNlxNH'),(50,'SALE','#9689fa','#ffffff',1,2,'a:2:{i:0;s:4:\"dn6d\";i:1;s:4:\"dn6q\";}',3,'366w','accesstoken_iYZkp8iOVeDHAvMJFEq4ytnotNlxNH');
/*!40000 ALTER TABLE `badges` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-20 10:18:07
