-- phpMyAdmin SQL Dump
-- version 4.2.9.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2015 at 02:33 PM
-- Server version: 5.6.22
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `badges`
--

-- --------------------------------------------------------

--
-- Table structure for table `badges`
--

CREATE TABLE IF NOT EXISTS `badges` (
`id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `b_color` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'ff0000',
  `t_color` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'ffffff',
  `enable` tinyint(1) NOT NULL DEFAULT '0',
  `counts` int(5) NOT NULL DEFAULT '0',
  `id_prod` text CHARACTER SET utf8 NOT NULL,
  `elem` int(2) NOT NULL DEFAULT '1',
  `id_mag` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `access_token` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_at` timestamp NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `badges`
--

INSERT INTO `badges` (`id`, `name`, `b_color`, `t_color`, `enable`, `counts`, `id_prod`, `elem`, `id_mag`, `access_token`, `create_at`, `modify_at`) VALUES
(18, '5', '#000000', '#ffffff', 1, 0, '', 2, '', '', '2015-01-22 12:35:30', '2015-01-22 12:36:29'),
(45, 'Top sale', '#000000', '#ffffff', 0, 0, '', 3, '', '', '2015-01-22 12:35:30', '2015-01-22 12:36:29'),
(46, 'best sale', '#000000', '#ffffff', 1, 0, '', 3, '', '', '2015-01-22 12:35:30', '2015-01-22 12:36:29'),
(48, 'HOT!', '#dbd2d2', '#0d0404', 0, 1, 'a:1:{i:0;s:4:"eEKW";}', 2, '366w', 'accesstoken_F1vmT0OM1zcMqQFMV38Hvgs4kPauSD', '2015-01-23 11:25:58', '2015-01-23 11:25:58'),
(49, 'NEW', '#ff4a4a', '#1e00ff', 1, 2, 'a:2:{i:0;s:4:"dn67";i:1;s:4:"dn6m";}', 1, '366w', 'accesstoken_8jVW4EaYIpnUI9tZMwimY3WiHVXRWF', '2015-01-22 12:35:30', '2015-01-23 08:36:04'),
(50, 'SALE', '#9689fa', '#cccccc', 1, 2, 'a:2:{i:0;s:4:"dn6d";i:1;s:4:"dn6q";}', 3, '366w', 'accesstoken_iYZkp8iOVeDHAvMJFEq4ytnotNlxNH', '2015-01-22 12:35:30', '2015-01-22 12:36:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `badges`
--
ALTER TABLE `badges`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `badges`
--
ALTER TABLE `badges`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
