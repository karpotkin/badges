<?php

require_once('View.php');


class AppView extends View
{

    function fetch()
    {
        $storeId = $this->request->get('storeId', 'string');
        $token = $this->request->get('access_token');
        $themeId = $this->request->get('themeId');

        $tictailProducts = $this->badges->getTictailProducts($storeId);

        $badges = $this->badges->getBadgesByStoreId($storeId);

        foreach($tictailProducts as $tp)
        {
            foreach($badges as $badge)
            {
                if(in_array($tp->id, $this->badges->getCurrentProducts($badge))){
                    $productsBadges[] = [
                        'product' => 'product/'.$tp->slug,
                        'b_color' => $badge->b_color,
                        't_color' => $badge->t_color,
                        'elem'    => $badge->elem,
                        'name'    => $badge->name,
                        'img_url' => substr($tp->images[0]->url, strrpos($tp[0]->images[0]->url, '/', -1) + 1)
                    ];
                }
            }
        }

        $theme = $this->badges->getCurrentTheme($storeId, $token); //не используется!

        $themeId = eregi_replace("([^0-9])", "", $themeId);

        if (!$themeId)
            $themeId = 231;

        foreach($productsBadges as $pb){
            $links[]['data'] = '<div class="ov_h" ' . (($pb['elem'] == 1) ? "style='overflow: hidden;'" : "") . '><div class="elem_' . $pb['elem'] . '_' . $themeId . ' text_bk" style="background-color:' . $pb['b_color'] . ';color:' . $pb['t_color'] . ';">' . $pb['name'] . '</div></div>';
            $links[]['slu'] = $pb['product'];
            $links[]['id'] = $pb['elem'];
            $links[]['img_url'] = $pb['img_url'];
            $links[]['theme_id'] = $themeId;
        }

        print json_encode($links);
    }

}