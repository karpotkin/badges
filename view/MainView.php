<?PHP
 
require_once('View.php');


class MainView extends View
{

	function fetch()
	{
		$this->design->assign('page_name', 'Your badges');
		$this->design->assign('meta_title', 'Badges - Tictail App');
		$this->design->assign('meta_keywords', '2');
		$this->design->assign('meta_description', '3');

		return $this->design->fetch('main.tpl');
	}
}
