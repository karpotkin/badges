<?PHP

require_once('View.php');
class IndexView extends View
{
	public $modules_dir = 'view/';

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * Отображение
	 *
	 */
	function fetch()
	{
		// Текущий модуль (для отображения центрального блока)
		$module = $this->request->get('module', 'string');
		$module = preg_replace("/[^A-Za-z0-9]+/", "", $module);

		if(empty($module))
			return false;

		// Создаем соответствующий класс
		if (is_file($this->modules_dir."$module.php"))
		{
			include_once($this->modules_dir."$module.php");
			if (class_exists($module))
			{
				$this->main = new $module($this);
			} else return false;
		} else return false;

		// Создаем основной блок страницы
		if (!$content = $this->main->fetch())
		{
			return false;
		}

		// Передаем основной блок в шаблон
		$this->design->assign('content', $content);

		$this->design->assign('module', $module);

		return $this->body = $content;
	}
}
