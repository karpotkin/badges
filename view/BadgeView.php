<?PHP
 
require_once('View.php');


class BadgeView extends View
{

	function fetch()
	{
		$id = $this->request->get('id');
		$storeId = $this->request->get('storeId', 'string');
		$token = $this->request->get('accessToken');

		$tictailProducts = $this->badges->getTictailProducts($storeId);

		$badge = $this->badges->getBadgeById($id);

		$badge = $this->badges->addCountProductsOnBadge($badge, $tictailProducts);
		$categories = $this->badges->getProductsCategories($tictailProducts);

		$currentProducts = $this->badges->getCurrentProducts($badge);

		$inArrayList = [];

		foreach($tictailProducts as $key => $tp){
			if(in_array($tp->id, $currentProducts)){
				$inArrayList[] = $key;
			}
		}

		$productsNotInCurrent = $this->badges->getProductsNotInCurrent($storeId, $id);

		if($this->request->method('post')){
			$me = $this->badges->getTictailStoreId($token);

			if($this->request->post('del')) {

				if($storeId == $me->id){
					$this->badges->deleteBadge($id);
				}

				header('Location: /');
			}

			$elem = 1;

			switch($this->request->post('elem_r'))
			{
				case "1":
					$elem = 1;
					break;
				case "2":
					$elem = 2;
					break;
				case "3":
					$elem = 3;
					break;
			}

			if($this->request->post('enable') == "on")
				$enable = true;
			else
				$enable = false;

			$c = 0;
			if($this->request->post("t_check"))
			{
				$array_check = $this->request->post("t_check");
				$c = count($array_check);

				if($this->request->post("check_val"))
				{
					$check_val = explode(",",$this->request->post("check_val"));
					$array_check = array_values(array_unique(array_merge($array_check, $check_val)));
					$c = count($array_check);
				}

				$id_prod = serialize($array_check);

			}

			$badge->id = $id;
			$badge->name = $this->request->post("title");
			$badge->b_color = $this->request->post('b_color');
			$badge->t_color = $this->request->post('t_color');
			$badge->enable = intval($enable);
			$badge->counts = intval($c);
			$badge->id_prod = $id_prod;
			$badge->elem = intval($elem);
			$badge->id_mag = $storeId;
			$badge->access_token = $token;

			if($id) {
				$this->badges->updateBadge($id, $badge);
			}
			else
			{
				$this->badges->addBadge($badge);
			}

			header('Location: /');

		}
		$this->design->assign('inArrayList', $inArrayList);
		$this->design->assign('categories', $categories);
		$this->design->assign('badge', $badge);
		$this->design->assign('token', $this->request->get('accessToken'));
		$this->design->assign('tictailProducts', $tictailProducts);
		$this->design->assign('productsNotInCurrent', $productsNotInCurrent);
		$this->design->assign('currentProducts', $currentProducts);

		$this->design->assign('meta_title', 'Badges - Tictail App');
		$this->design->assign('meta_keywords', '2');
		$this->design->assign('meta_description', '3');

		return $this->design->fetch('badge.tpl');
	}
}
