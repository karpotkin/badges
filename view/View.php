<?PHP

require_once('classes/Master.php');

class View extends Master
{
	/* Класс View похож на синглтон, храним статически его инстанс */

	private static $view_instance;
	
	public function __construct()
	{
		parent::__construct();

		if(self::$view_instance)
		{

		}
		else
		{
			// Сохраняем свой инстанс в статической переменной,
			// чтобы в следующий раз использовать его
			self::$view_instance = $this;

		}
	}
		
	/**
	 *
	 * Отображение
	 *
	 */
	function fetch()
	{
		return false;
	}
}
